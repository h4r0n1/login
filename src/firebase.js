import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyA7BG2-udepnAA0fwuPPmX0t4sJpcQMPy0",
  authDomain: "login-e54ea.firebaseapp.com",
  projectId: "login-e54ea",
  storageBucket: "login-e54ea.appspot.com",
  messagingSenderId: "93507026079",
  appId: "1:93507026079:web:8285fe6e754027ccceb6aa",
};

const fire = firebase.initializeApp(firebaseConfig);

export default fire;